package com.chrisgammage.gwtmvpexample.client;

import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.web.bindery.event.shared.EventBus;

import javax.inject.Named;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:05 PM
 */
@GinModules(MyGinModule.class)
public interface MyInjector extends Ginjector {
  // A global event bus
  EventBus eventBus();

  ActivityMapper activityMapper();
  PlaceHistoryMapper placeHistoryMapper();
  PlaceController placeController();

  // Get the starting place, set from within the GIN Module
  @Named("defaultPlace") Place defaultPlace();
}
