package com.chrisgammage.gwtmvpexample.client.main_page;

import com.chrisgammage.gwtmvpexample.client.common.Model;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 11:15 PM
 */
public interface MainPageModel extends Model {
  String getSelectedTab();
  void setSelectedTab(String selectedTab);
}
