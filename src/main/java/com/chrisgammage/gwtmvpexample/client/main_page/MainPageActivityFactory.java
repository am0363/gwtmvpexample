package com.chrisgammage.gwtmvpexample.client.main_page;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 11:13 PM
 */
public interface MainPageActivityFactory {
  MainPageActivity createMainPageActivity(MainPagePlace place);
}
