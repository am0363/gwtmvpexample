package com.chrisgammage.gwtmvpexample.client.main_page;

import com.chrisgammage.gwtmvpexample.client.common.Presenter;
import com.chrisgammage.gwtmvpexample.client.common.View;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:51 PM
 */
public interface MainPagePresenter extends Presenter {
}
