package com.chrisgammage.gwtmvpexample.client.main_page;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:50 PM
 */
public class MainPagePlace extends Place {

  private String selectedTab;

  public MainPagePlace() {
    selectedTab = "about";
  }

  public MainPagePlace(String selectedTab) {
    this.selectedTab = selectedTab;
  }

  public String getSelectedTab() {
    return selectedTab;
  }

  public static class Tokenizer implements PlaceTokenizer<MainPagePlace> {

    @Override
    public MainPagePlace getPlace(String s) {
      return new MainPagePlace(s);
    }
    @Override
    public String getToken(MainPagePlace mainPagePlace) {
      return mainPagePlace.getSelectedTab();
    }
  }
}
