package com.chrisgammage.gwtmvpexample.client.main_page;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 11:05 PM
 */
public class MainPageActivity extends AbstractActivity {

  @Inject MainPagePresenter mainPagePresenter;
  @Inject MainPageModel mainPageModel;
  private MainPagePlace place;

  @Inject
  public MainPageActivity(@Assisted MainPagePlace place) {
    this.place = place;
  }

  @Override
  public void start(AcceptsOneWidget acceptsOneWidget, EventBus eventBus) {
    mainPageModel.setSelectedTab(place.getSelectedTab());
    acceptsOneWidget.setWidget(mainPagePresenter.getView());
  }
}
