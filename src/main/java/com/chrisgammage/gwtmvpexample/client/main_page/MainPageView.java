package com.chrisgammage.gwtmvpexample.client.main_page;

import com.chrisgammage.gwtmvpexample.client.common.View;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.SourcesTabEvents;
import com.google.gwt.user.client.ui.Widget;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:51 PM
 */
public interface MainPageView extends View {
  void addTab(String name, Widget content);
  void setSelectedTab(String name);
  HasSelectionHandlers<String> getTabEventsSource();
}
