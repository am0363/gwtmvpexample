package com.chrisgammage.gwtmvpexample.client.main_page.impl;

import com.chrisgammage.gwtmvpexample.client.main_page.MainPageView;
import com.google.gwt.core.client.GWT;

import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TabBar;
import com.google.gwt.user.client.ui.Widget;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 11:01 PM
 */
public class MainPageViewImpl extends Composite implements MainPageView {

  interface MainPageViewImplUiBinder extends UiBinder<Widget, MainPageViewImpl> {
  }

  private static MainPageViewImplUiBinder ourUiBinder = GWT.create(MainPageViewImplUiBinder.class);
  private Map<String, Widget> tabs = new HashMap<String, Widget>();
  @UiField TabBar tabBar;
  @UiField SimplePanel tabContent;

  private HasSelectionHandlers<String> mySelectionSource = new HasSelectionHandlers<String>() {
    private HandlerManager handlerManager = new HandlerManager(this);
    @Override
    public HandlerRegistration addSelectionHandler(SelectionHandler<String> stringSelectionHandler) {
      return handlerManager.addHandler(SelectionEvent.getType(), stringSelectionHandler);
    }

    @Override
    public void fireEvent(GwtEvent<?> gwtEvent) {
      handlerManager.fireEvent(gwtEvent);
    }
  };

  @UiHandler("tabBar")
  void onTabSelection(SelectionEvent<Integer> event) {
    String name = tabBar.getTabHTML(event.getSelectedItem());
    SelectionEvent.fire(mySelectionSource, name);
  }

  public MainPageViewImpl() {
    initWidget(ourUiBinder.createAndBindUi(this));
  }

  @Override
  public void addTab(String name, Widget content) {
    tabs.put(name, content);
    tabBar.addTab(name);
  }

  @Override
  public void setSelectedTab(String name) {
    tabContent.setWidget(tabs.get(name));
  }

  @Override
  public HasSelectionHandlers<String> getTabEventsSource() {
    return mySelectionSource;
  }
}