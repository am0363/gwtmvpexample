package com.chrisgammage.gwtmvpexample.client.main_page.impl;

import com.chrisgammage.gwtmvpexample.client.MyPlaceController;
import com.chrisgammage.gwtmvpexample.client.common.Model;
import com.chrisgammage.gwtmvpexample.client.common.ModelEventHandler;
import com.chrisgammage.gwtmvpexample.client.common.View;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageModel;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPagePlace;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPagePresenter;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageView;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:53 PM
 */
public class MainPagePresenterImpl implements MainPagePresenter, ModelEventHandler, SelectionHandler<String> {

  private MainPageView view;
  private MainPageModel model;
  @Inject PlaceController placeController;

  @Inject
  public MainPagePresenterImpl(MainPageModel model, MainPageView view) {
    this.model = model;
    this.view = view;
    view.addTab("about", new Label("about"));
    view.addTab("test", new Label("test"));
    model.addModelEventHandler(this);
    view.getTabEventsSource().addSelectionHandler(this);
  }

  @Override
  public View getView() {
    return view;
  }

  @Override
  public void onModelPropertyChanged(String property, Model model) {
    if("selectedTab".equals(property)) {
      view.setSelectedTab(this.model.getSelectedTab());
    }
  }

  @Override
  public void onSelection(SelectionEvent<String> stringSelectionEvent) {
    placeController.goTo(new MainPagePlace(stringSelectionEvent.getSelectedItem()));
  }
}
