package com.chrisgammage.gwtmvpexample.client.main_page.impl;

import com.chrisgammage.gwtmvpexample.client.common.ModelBase;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageModel;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/6/12
 * Time: 10:34 PM
 */
public class MainPageModelImpl extends ModelBase implements MainPageModel {

  private String selectedTab;

  @Override
  public String getSelectedTab() {
    return selectedTab;
  }

  @Override
  public void setSelectedTab(String selectedTab) {
    this.selectedTab = selectedTab;
    onPropertyChanged("selectedTab");
  }
}
