package com.chrisgammage.gwtmvpexample.client;

import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageActivity;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPagePlace;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageActivity;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageActivityFactory;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPagePlace;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:21 PM
 */
public class MyActivityMapper implements ActivityMapper {

  @Inject Provider<LoginPageActivity> loginPageActivityProvider;
  @Inject MainPageActivityFactory mainPageActivityFactory;

  @Override
  public Activity getActivity(Place place) {
    if(place instanceof LoginPagePlace) {
      return loginPageActivityProvider.get();
    } else if(place instanceof MainPagePlace) {
      return mainPageActivityFactory.createMainPageActivity((MainPagePlace)place);
    }
    return null;
  }
}
