package com.chrisgammage.gwtmvpexample.client;

import com.chrisgammage.gwtmvpexample.client.actions.LoginAction;
import com.chrisgammage.gwtmvpexample.client.common.Action;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageModel;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPagePlace;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPagePresenter;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageView;
import com.chrisgammage.gwtmvpexample.client.login_page.impl.LoginPageModelImpl;
import com.chrisgammage.gwtmvpexample.client.login_page.impl.LoginPagePresenterImpl;
import com.chrisgammage.gwtmvpexample.client.login_page.impl.LoginPageViewImpl;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageActivityFactory;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageModel;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPagePresenter;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPageView;
import com.chrisgammage.gwtmvpexample.client.main_page.impl.MainPageModelImpl;
import com.chrisgammage.gwtmvpexample.client.main_page.impl.MainPagePresenterImpl;
import com.chrisgammage.gwtmvpexample.client.main_page.impl.MainPageViewImpl;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.inject.name.Names;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:07 PM
 */
public class MyGinModule extends AbstractGinModule {
  @Override
  protected void configure() {
    // Global Event Bus
    bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);

    // Set up singletons of Place Objects
    bind(PlaceController.class).to(MyPlaceController.class).in(Singleton.class);
    bind(PlaceHistoryMapper.class).to(MyPlaceHistoryMapper.class).in(Singleton.class);
    bind(Place.class).annotatedWith(Names.named("defaultPlace")).to(LoginPagePlace.class);

    // Set up Activity Handling Object
    bind(ActivityMapper.class).to(MyActivityMapper.class).in(Singleton.class);

    // Setup the Objects required by the Login Page
    bind(LoginPagePresenter.class).to(LoginPagePresenterImpl.class).in(Singleton.class);
    bind(LoginPageModel.class).to(LoginPageModelImpl.class).in(Singleton.class);
    bind(LoginPageView.class).to(LoginPageViewImpl.class).in(Singleton.class);
    bind(Action.class).annotatedWith(Names.named("loginAction")).to(LoginAction.class);

    // Setup the Objects required by the MainPage
    bind(MainPagePresenter.class).to(MainPagePresenterImpl.class).in(Singleton.class);
    bind(MainPageView.class).to(MainPageViewImpl.class).in(Singleton.class);
    bind(MainPageModel.class).to(MainPageModelImpl.class).in(Singleton.class);

    // We need to pass a non-gin parameter to MainPage activity in order to
    // give it the place information (selected tab).
    install(new GinFactoryModuleBuilder().build(MainPageActivityFactory.class));
  }
}
