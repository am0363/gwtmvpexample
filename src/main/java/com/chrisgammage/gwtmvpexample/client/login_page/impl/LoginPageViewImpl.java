package com.chrisgammage.gwtmvpexample.client.login_page.impl;

import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:18 PM
 */
public class LoginPageViewImpl extends Composite implements LoginPageView {

  interface LoginPageViewImplUiBinder extends UiBinder<Widget, LoginPageViewImpl> {
  }

  private static LoginPageViewImplUiBinder ourUiBinder = GWT.create(LoginPageViewImplUiBinder.class);

  @UiField TextBox userName;
  @UiField TextBox password;
  @UiField Button loginButton;

  public LoginPageViewImpl() {
    initWidget(ourUiBinder.createAndBindUi(this));
  }

  @Override
  public HasClickHandlers getLoginButton() {
    return loginButton;
  }

  @Override
  public HasValue<String> getUserNameField() {
    return userName;
  }

  @Override
  public HasValue<String> getPasswordField() {
    return password;
  }
}