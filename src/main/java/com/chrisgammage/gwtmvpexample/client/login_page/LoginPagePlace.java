package com.chrisgammage.gwtmvpexample.client.login_page;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:14 PM
 */
public class LoginPagePlace extends Place {

  public static class Tokenizer implements PlaceTokenizer<LoginPagePlace> {
    @Override
    public LoginPagePlace getPlace(String s) {
      return new LoginPagePlace();
    }
    @Override
    public String getToken(LoginPagePlace loginPagePlace) {
      return null;
    }
  }
}
