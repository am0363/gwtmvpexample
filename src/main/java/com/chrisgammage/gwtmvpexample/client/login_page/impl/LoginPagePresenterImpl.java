package com.chrisgammage.gwtmvpexample.client.login_page.impl;

import com.chrisgammage.gwtmvpexample.client.common.Action;
import com.chrisgammage.gwtmvpexample.client.common.ActionCallback;
import com.chrisgammage.gwtmvpexample.client.common.View;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageModel;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPagePresenter;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:17 PM
 */
public class LoginPagePresenterImpl implements LoginPagePresenter, ClickHandler {

  private LoginPageView view;
  @Inject LoginPageModel model;

  @Inject @Named("loginAction") Action loginAction;

  @Inject
  public LoginPagePresenterImpl(LoginPageView view) {
    this.view = view;
    view.getLoginButton().addClickHandler(this);
  }

  @Override
  public View getView() {
    return view;
  }

  @Override
  public void onClick(ClickEvent clickEvent) {
    model.setPassword(view.getPasswordField().getValue());
    model.setUserName(view.getUserNameField().getValue());
    loginAction.start(ActionCallback.EMPTY_CALLBACK);
  }
}
