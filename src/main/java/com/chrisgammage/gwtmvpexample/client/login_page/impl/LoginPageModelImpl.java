package com.chrisgammage.gwtmvpexample.client.login_page.impl;

import com.chrisgammage.gwtmvpexample.client.common.ModelBase;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageModel;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:54 PM
 */
public class LoginPageModelImpl extends ModelBase implements LoginPageModel {

  private String userName;
  private String password;

  @Override
  public String getUserName() {
    return userName;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public void setUserName(String userName) {
    this.userName = userName;
  }

  @Override
  public void setPassword(String password) {
    this.password = password;
  }
}
