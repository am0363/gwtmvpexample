package com.chrisgammage.gwtmvpexample.client.login_page;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:25 PM
 */
public class LoginPageActivity extends AbstractActivity {

  @Inject LoginPagePresenter loginPagePresenter;

  @Override
  public void start(AcceptsOneWidget acceptsOneWidget, EventBus eventBus) {
    acceptsOneWidget.setWidget(loginPagePresenter.getView());
  }
}
