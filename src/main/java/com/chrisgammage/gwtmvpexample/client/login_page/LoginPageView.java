package com.chrisgammage.gwtmvpexample.client.login_page;

import com.chrisgammage.gwtmvpexample.client.common.View;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasValue;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:17 PM
 */
public interface LoginPageView extends View {
  HasClickHandlers getLoginButton();
  HasValue<String> getUserNameField();
  HasValue<String> getPasswordField();
}
