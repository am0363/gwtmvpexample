package com.chrisgammage.gwtmvpexample.client.login_page;

import com.chrisgammage.gwtmvpexample.client.common.Model;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:54 PM
 */
public interface LoginPageModel extends Model {
  String getUserName();
  String getPassword();
  void setUserName(String userName);
  void setPassword(String password);
}
