package com.chrisgammage.gwtmvpexample.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.web.bindery.event.shared.EventBus;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 9:27 PM
 */
public class GWTMVPExample implements EntryPoint {

  private static final Logger LOG = Logger.getLogger(GWTMVPExample.class.getName());
  private MyInjector inject = GWT.create(MyInjector.class);
  private SimplePanel appWidget = new SimplePanel();

  public void onModuleLoad() {

    // We are going to delay the application start and add an uncaught exception
    // handler, for any exceptions we might otherwise miss
    GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
      @Override
      public void onUncaughtException(Throwable throwable) {
        LOG.log(Level.SEVERE, "Uncaught GWT Exception", throwable);
      }
    });
    Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
      @Override
      public void execute() {
        onModuleLoad2();
      }
    });
  }

  private void onModuleLoad2() {

    // Our event bus in GIN is a Singleton so we can access it from anywhere
    EventBus eventBus = inject.eventBus();

    // We set up our activity handling
    ActivityManager activityManager = new ActivityManager(inject.activityMapper(),
            eventBus);
    activityManager.setDisplay(appWidget);

    // Set up our Place handling, to remember where we've been
    PlaceHistoryHandler placeHistoryHandler =
            new PlaceHistoryHandler(inject.placeHistoryMapper());

    // Register the place handler with a default place
    placeHistoryHandler.register(inject.placeController(), eventBus,
            inject.defaultPlace());

    // add a simple panel to contain our active view.
    RootPanel.get().add(appWidget);

    placeHistoryHandler.handleCurrentHistory();
  }
}
