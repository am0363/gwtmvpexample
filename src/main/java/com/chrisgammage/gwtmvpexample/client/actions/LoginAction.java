package com.chrisgammage.gwtmvpexample.client.actions;

import com.chrisgammage.gwtmvpexample.client.common.Action;
import com.chrisgammage.gwtmvpexample.client.common.ActionCallback;
import com.chrisgammage.gwtmvpexample.client.login_page.LoginPageModel;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPagePlace;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:44 PM
 */
public class LoginAction implements Action {

  @Inject PlaceController placeController;
  @Inject LoginPageModel loginPageModel;

  @Override
  public void start(final ActionCallback callback) {
    validateUserNameAndPassword(loginPageModel.getUserName(), loginPageModel.getPassword(), new AsyncCallback<Boolean>() {
      @Override
      public void onFailure(Throwable throwable) {
        // display error?
        callback.actionFinished();
      }
      @Override
      public void onSuccess(Boolean aBoolean) {
        if(aBoolean) {
          placeController.goTo(new MainPagePlace());
        } else {
          // notify login failed
        }
        callback.actionFinished();
      }
    });
  }

  private void validateUserNameAndPassword(String userName, String password, AsyncCallback<Boolean> callback) {
    callback.onSuccess(true);
  }
}
