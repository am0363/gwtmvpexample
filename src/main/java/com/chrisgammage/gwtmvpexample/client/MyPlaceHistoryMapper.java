package com.chrisgammage.gwtmvpexample.client;

import com.chrisgammage.gwtmvpexample.client.login_page.LoginPagePlace;
import com.chrisgammage.gwtmvpexample.client.main_page.MainPagePlace;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:22 PM
 */
@WithTokenizers({LoginPagePlace.Tokenizer.class, MainPagePlace.Tokenizer.class})
public interface MyPlaceHistoryMapper extends PlaceHistoryMapper {
}
