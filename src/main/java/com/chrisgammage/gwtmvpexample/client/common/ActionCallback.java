package com.chrisgammage.gwtmvpexample.client.common;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:44 PM
 */
public interface ActionCallback {

  static public ActionCallback EMPTY_CALLBACK = new ActionCallback() {
    @Override
    public void actionFinished() {
    }
  };

  void actionFinished();
}
