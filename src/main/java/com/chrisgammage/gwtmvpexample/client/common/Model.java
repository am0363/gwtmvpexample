package com.chrisgammage.gwtmvpexample.client.common;

import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/6/12
 * Time: 10:48 PM
 */
public interface Model {
  HandlerRegistration addModelEventHandler(ModelEventHandler modelHandler);
}
