package com.chrisgammage.gwtmvpexample.client.common;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/6/12
 * Time: 10:50 PM
 */
public class ModelEvent extends GwtEvent<ModelEventHandler> {
  public static Type<ModelEventHandler> TYPE = new Type<ModelEventHandler>();

  private final Model model;
  private final String property;

  public ModelEvent(String property, Model model) {
    this.property = property;
    this.model = model;
  }

  public Type<ModelEventHandler> getAssociatedType() {
    return TYPE;
  }

  protected void dispatch(ModelEventHandler handler) {
    handler.onModelPropertyChanged(property, model);
  }
}
