package com.chrisgammage.gwtmvpexample.client.common;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/6/12
 * Time: 10:50 PM
 */
public interface ModelEventHandler extends EventHandler {
  void onModelPropertyChanged(String property, Model model);
}
