package com.chrisgammage.gwtmvpexample.client.common;

import com.google.gwt.event.shared.HandlerManager;
import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/6/12
 * Time: 10:49 PM
 */
public class ModelBase implements Model {

  private HandlerManager handlerManager = new HandlerManager(this);

  @Override
  public HandlerRegistration addModelEventHandler(ModelEventHandler modelHandler) {
    return handlerManager.addHandler(ModelEvent.TYPE, modelHandler);
  }

  protected void onPropertyChanged(String property) {
    handlerManager.fireEvent(new ModelEvent(property, this));
  }
}
