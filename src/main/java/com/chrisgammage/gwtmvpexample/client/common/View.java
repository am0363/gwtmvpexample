package com.chrisgammage.gwtmvpexample.client.common;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:28 PM
 */
public interface View extends IsWidget {
}
