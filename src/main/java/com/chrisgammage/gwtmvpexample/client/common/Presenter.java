package com.chrisgammage.gwtmvpexample.client.common;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:28 PM
 */
public interface Presenter {
  View getView();
}
