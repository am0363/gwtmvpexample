package com.chrisgammage.gwtmvpexample.client;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/5/12
 * Time: 10:10 PM
 */
public class MyPlaceController extends PlaceController {
  @Inject
  public MyPlaceController(EventBus eventBus) {
    super(eventBus);
  }
}
